#!/usr/bin/python3

import sys
import imutils

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import QTimer
from PyQt5.Qt import Qt

import cv2

from UI_camera import *


class CameraWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.timer = QTimer()
        self.timer.timeout.connect(self.viewCam)
        # self.ui.camera360_button.clicked.connect(self.camera360)
        self.ui.logitech_button.clicked.connect(self.logitech)
        self.ui.thermal_button.clicked.connect(self.thermal)

    def viewCam(self):
        _, image = self.cap.read()
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        img = imutils.resize(image, width=480)
        height, width, channel = img.shape
        step = channel * width
        qImg = QImage(img.data, width, height, step, QImage.Format_RGB888)
        self.ui.label.setPixmap(QPixmap.fromImage(qImg))

    # def camera360(self):
    #     if not self.timer.isActive():
    #         self.cap = cv2.VideoCapture(0)
    #         self.timer.start(1)

    def logitech(self):
        if not self.timer.isActive():
            self.cap = cv2.VideoCapture(0)
            self.timer.start(1)
        else:
            # stop timer
            self.timer.stop()
            # release video capture
            self.cap.release()

    def thermal(self):
        if not self.timer.isActive():
            self.cap = cv2.VideoCapture(2)
            self.timer.start(1)
        else:
            # stop timer
            self.timer.stop()
            # release video capture
            self.cap.release()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    cameraWindow = CameraWindow()
    cameraWindow.show()
    sys.exit(app.exec_())
