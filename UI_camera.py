# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'camera.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(719, 551)
        self.logitech_button = QtWidgets.QPushButton(Form)
        self.logitech_button.setGeometry(QtCore.QRect(70, 470, 151, 61))
        self.logitech_button.setObjectName("logitech_button")
        self.thermal_button = QtWidgets.QPushButton(Form)
        self.thermal_button.setGeometry(QtCore.QRect(290, 470, 151, 61))
        self.thermal_button.setObjectName("thermal_button")
        self.camera360_button = QtWidgets.QPushButton(Form)
        self.camera360_button.setGeometry(QtCore.QRect(500, 470, 151, 61))
        self.camera360_button.setObjectName("camera360_button")
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(20, 60, 671, 401))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.back_button = QtWidgets.QPushButton(Form)
        self.back_button.setGeometry(QtCore.QRect(50, 20, 89, 25))
        self.back_button.setObjectName("back_button")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Jomblo Detection"))
        self.logitech_button.setText(_translate("Form", "Camera Logitech"))
        self.thermal_button.setText(_translate("Form", "Camera Thermal"))
        self.camera360_button.setText(_translate("Form", "Camera 360"))
        self.label.setText(_translate("Form", "MODE CAMERA"))
        self.back_button.setText(_translate("Form", "<-  Back"))
