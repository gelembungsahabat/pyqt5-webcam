import cv2
import sys
import imutils
import serial

from UI_main import *
from PyQt5.Qt import Qt
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QImage
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QApplication

serialData = serial.Serial('/dev/ttyUSB0')


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.timer = QTimer()
        self.timer.timeout.connect(self.viewCam)
        self.controlTimer()
        self.ui.pushButton_4.clicked.connect(self.stop)
        self.ui.pushButton_5.clicked.connect(self.stop)
        self.ui.pushButton_6.clicked.connect(self.stop)
        self.ui.pushButton_7.clicked.connect(self.stop)

    def viewCam(self):
        ret, image = self.cap.read()
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        img = imutils.resize(image, width=480)
        height, width, channel = img.shape
        step = channel * width
        qImg = QImage(img.data, width, height, step, QImage.Format_RGB888)
        self.ui.label.setPixmap(QPixmap.fromImage(qImg))

    def controlTimer(self):
        if not self.timer.isActive():
            self.cap = cv2.VideoCapture(0)
            self.timer.start(1)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

    def keyReleaseEvent(self, event):
        if event.key() == Qt.Key_Up:
            print("Maju")
            data = str("w")
            serialData.write(data.encode())
        elif event.key() == Qt.Key_Right:
            print("Kanan")
            data = str("d")
            serialData.write(data.encode())
        elif event.key() == Qt.Key_Down:
            print("Mundur")
            data = str("s")
            serialData.write(data.encode())
        elif event.key() == Qt.Key_Left:
            print("Kiri")
            data = str("a")
            serialData.write(data.encode())

    def stop(self):
        print("stop!")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())
